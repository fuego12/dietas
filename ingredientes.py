huevo = {
'nombre': 'huevo',
'peso': 65.0,
'datos': {
	'prot': 13.0,
	'kcal': 155.0,
	'fat': 11.0,
	'carbs': 1.1,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

leche1 = {
'nombre': 'Leche 1%',
'peso': 100.0,
'datos': {
	'prot': 3.0,
	'kcal': 40.0,
	'fat': 1.0,
	'carbs': 4.8,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

pollo_pechuga = {
'nombre': 'pollo_pechuga',
'peso': 100.0,
'datos': {
	'prot': 29.55,
	'kcal': 195.,
	'fat': 7.72,
	'carbs': 0.,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

manzana = {
'nombre': 'manzana',
'peso': 100.0,
'datos': {
	'prot': 0.26,
	'kcal': 52,
	'fat': 0.17,
	'carbs': 13.81,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

queso_cremoso = {
'nombre': 'queso_cremoso',
'peso': 100.0,
'datos': {
	'prot': 18.6,
	'kcal': 301.,
	'fat': 24.99,
	'carbs': 0.45,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

arroz = {
'nombre': 'arroz',
'peso': 100.0,
'datos': {
	'prot': 2.7,
	'kcal': 130.0,
	'fat': 0.3,
	'carbs': 28.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

#############################################

bife_de_chorizo = {
'nombre': 'Bife de Chorizo',
'peso': 100.0,
'datos': {
	'prot': 20.47,
	'kcal': 142.0,
	'fat': 6.02,
	'carbs': 0.0,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

yogurt_bebible = {
'nombre': 'Yogurt Bebible (Yogurisimo)',
'peso': 100.0,
'datos': {
	'prot': 5.6/2,
	'kcal': 117/2,
	'fat': 0.6,
	'carbs': 10.5,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

yogurt_firme = {
'nombre': 'Yogurt Firme (La Serenisima)',
'peso': 190.0,
'datos': {
	'prot': 6.6,
	'kcal': 132.0,
	'fat': 1.9,
	'carbs': 22.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

milanesa_de_merluza = {
'nombre': 'Milanesa de merluza',
'peso': 100.0,
'datos': {
	'prot': 18.14,
	'kcal': 147.0,
	'fat': 4.36,
	'carbs': 7.51,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

atun_natural = {
'nombre': 'Atun natural (La Campagnola)',
'peso': 120.0,
'datos': {
	'prot': 7.4*2,
	'kcal': 46.0*2,
	'fat': 0.7*2,
	'carbs': 2.4*2,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

atun_en_aceite = {
'nombre': 'atun_en_aceite (La Campagnola)',
'peso': 120.0,
'datos': {
	'prot': 14.0,
	'kcal': 106.0,
	'fat': 4.0,
	'carbs': 2.5,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

nuggets = {
'nombre': 'Nuggets de Pollo (Swift)',
'peso': 100.0,
'datos': {
	'prot': 25/1.30,
	'kcal': 360/1.30,
	'fat': 14/1.30,
	'carbs': 33/1.30,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

fideos = {
'nombre': 'Fideos',
'peso': 100.0,
'datos': {
	'prot': 4.51,
	'kcal': 137.0,
	'fat': 2.06,
	'carbs': 25.01,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

alfajor = {
'nombre': 'Alfajor',
'peso': 100.0,
'datos': {
	'prot': 3.5,
	'kcal': 205.0,
	'fat': 7.4,
	'carbs': 30.0,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

chocolate_aguila_semiamargo = {
'nombre': 'Barrita de Chocolate Aguila Semiamargo',
'peso': 21.0,
'datos': {
	'prot': 1.3,
	'kcal': 98.0,
	'fat': 5.1,
	'carbs': 13.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

chocolate_aguila_70 = {
'nombre': 'Barrita de Chocolate Aguila 70% Cacao',
'peso': 21.0,
'datos': {
	'prot': 1.8,
	'kcal': 118.0,
	'fat': 8.8,
	'carbs': 7.9,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

helado_alto = {
'nombre': 'Helado de tramontana',
'peso': 100.0,
'datos': {
	'prot': 3.0/0.6,
	'kcal': 147.0/0.6,
	'fat': 6.0/0.6,
	'carbs': 19.0/0.5,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

helado_bajo = {
'nombre': 'Helado de dulce de leche',
'peso': 100.0,
'datos': {
	'prot': 3.2/0.6,
	'kcal': 110.0/0.6,
	'fat': 3.7/0.6,
	'carbs': 16.0/0.6,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

milanesa_de_carne = {
'nombre': 'Milanesa de carne',
'peso': 100.0,
'datos': {
	'prot': 27.29,
	'kcal': 228.0,
	'fat': 9.18,
	'carbs': 9.85,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

milanesa_de_pollo = {
'nombre': 'Milanesa de pollo',
'peso': 100.0,
'datos': {
	'prot': 25.77,
	'kcal': 222.0,
	'fat': 9.09,
	'carbs': 7.55,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

chorizo = {
'nombre': 'Chorizo de cerdo',
'peso': 100.0,
'datos': {
	'prot': 19.43,
	'kcal': 339.0,
	'fat': 28.36,
	'carbs': 0.0,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

tira_de_asado = {
'nombre': 'Tira de asado',
'peso': 100.0,
'datos': {
	'prot': 21.57,
	'kcal': 471.0,
	'fat': 41.98,
	'carbs': 0.0,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

chinchulin = {
'nombre': 'Chinchulin',
'peso': 100.0,
'datos': {
	'prot': 11.97,
	'kcal': 84.0,
	'fat': 3.66,
	'carbs': 0.0,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

pan_lactal = {
'nombre': 'Pan lactal',
'peso': 100.0,
'datos': {
	'prot': 8.4,
	'kcal': 240.0,
	'fat': 1.0,
	'carbs': 50.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

pan_frances = {
'nombre': 'Pan frances',
'peso': 100.0,
'datos': {
	'prot': 8.8,
	'kcal': 274.0,
	'fat': 3.0,
	'carbs': 51.9,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

jugo_de_naranja = {
'nombre': 'Jugo de naranja ',
'peso': 100.0,
'datos': {
	'prot': 0.65,
	'kcal': 41.0,
	'fat': 0.0,
	'carbs': 9.5,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

frutos_secos = {
'nombre': 'Frutos secos',
'peso': 100.0,
'datos': {
	'prot': 16.51,
	'kcal': 617.0,
	'fat': 56.3,
	'carbs': 21.58,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

manteca = {
'nombre': 'Manteca (La Sarenisima)',
'peso': 100.0,
'datos': {
	'prot': 1.0,
	'kcal': 740.0,
	'fat': 82.0,
	'carbs': 0.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

dulce_de_leche = {
'nombre': 'Dulce de leche (clasico La Serenisima)',
'peso': 100.0,
'datos': {
	'prot': 5.5,
	'kcal': 65.0*5,
	'fat': 1.7*5,
	'carbs': 55.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

mermelada = {
'nombre': 'Chinchulin',
'peso': 100.0,
'datos': {
	'prot': 0.54,
	'kcal': 260.0,
	'fat': 0.14,
	'carbs': 66.63,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

ravioles_pollo_y_verdura = {
'nombre': 'Ravioles de pollo y verdura',
'peso': 100.0,
'datos': {
	'prot': 9.2,
	'kcal': 285.0,
	'fat': 7.0,
	'carbs': 46.0,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

ravioles_ricota = {
'nombre': 'Ravioles de ricota',
'peso': 100.0,
'datos': {
	'prot': 9.2,
	'kcal': 254.0,
	'fat': 7.2,
	'carbs': 38.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

ravioles_carne_y_verdura = {
'nombre': 'Ravioles de carne y verdura',
'peso': 100.0,
'datos': {
	'prot': 9.6/1.25,
	'kcal': 359.0/1.25,
	'fat': 7.1/1.25,
	'carbs': 64.0/1.25,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

ravioles_4_quesos = {
'nombre': 'Ravioles 4 quesos',
'peso': 100.0,
'datos': {
	'prot': 7.9,
	'kcal': 234.0,
	'fat': 2.9,
	'carbs': 44.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

pure_de_papa = {
'nombre': 'Pure de papa casero',
'peso': 100.0,
'datos': {
	'prot': 1.8,
	'kcal': 100.0,
	'fat': 3.54,
	'carbs': 15.72,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

pure_de_papa_knorr = {
'nombre': 'Pure de papa knorr',
'peso': 100.0,
'datos': {
	'prot': 1.9,
	'kcal': 67.0,
	'fat': 2.3,
	'carbs': 10.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

pure_de_calabaza = {
'nombre': 'Pure de calabaza casero',
'peso': 100.0,
'datos': {
	'prot': 1.19,
	'kcal': 74.0,
	'fat': 4.03,
	'carbs': 9.56,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

paty = {
'nombre': 'Hamburguesa (Paty)',
'peso': 80.0,
'datos': {
	'prot': 15.0,
	'kcal': 198.0,
	'fat': 15.0,
	'carbs': 0.0,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

hamburguesa = {
'nombre': 'Hamburguesa casera',
'peso': 100.0,
'datos': {
	'prot': 23.74,
	'kcal': 222.0,
	'fat': 11.29,
	'carbs': 5.1,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

cheddar = {
'nombre': 'Cheddar',
'peso': 100.0,
'datos': {
	'prot': 24.9,
	'kcal': 403.0,
	'fat': 33.14,
	'carbs': 1.28,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

morcilla = {
'nombre': 'Morcilla',
'peso': 100.0,
'datos': {
	'prot': 14.6,
	'kcal': 379.0,
	'fat': 34.5,
	'carbs': 1.29,
	},
'dieta': {
	'Veggie': False,
	'Vegan': False
	}
}

proteina_ENA = {
'nombre': 'Proteina ENA',
'peso': 30.0,
'datos': {
	'prot': 24.0,
	'kcal': 119.0,
	'fat': 2.4,
	'carbs': 2.6,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

proteina_Just_Whey = {
'nombre': 'Proteina sin sabor Star Nutrition',
'peso': 30.0,
'datos': {
	'prot': 25.0,
	'kcal': 123.0,
	'fat': 1.8,
	'carbs': 1.66,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

yogurade = {
'nombre': 'Yogurade',
'peso': 210.0,
'datos': {
	'prot': 17.02,
	'kcal': 157.0,
	'fat': 2.36,
	'carbs': 17.14,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

mayonesa = {
'nombre': 'Mayonesa',
'peso': 100.0,
'datos': {
	'prot': 0.9,
	'kcal': 390.0,
	'fat': 33.4,
	'carbs': 23.9,
	},
'dieta': {
	'Veggie': True,
	'Vegan': False
	}
}

choclo = {
'nombre': 'Proteina ENA',
'peso': 100.0,
'datos': {
	'prot': 3.3/1.3,
	'kcal': 77.0/1.3,
	'fat': 3.3/1.3,
	'carbs': 11.0/1.3,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

polenta = {
'nombre': 'Polenta',
'peso': 100.0,
'datos': {
	'prot': 7.0,
	'kcal': 340.0,
	'fat': 0.0,
	'carbs': 78.0,
	},
'dieta': {
	'Veggie': True,
	'Vegan': True
	}
}

