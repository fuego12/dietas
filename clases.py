from ingredientes import *
from sympy import Matrix, lambdify
from scipy.optimize import minimize
from numpy.linalg import norm
from numpy import ones, inf

class ingrediente: 
	cualidades = ['peso', 'prot', 'kcal', 'fat', 'carbs', 'Veggie', 'Vegan']
	def __init__(self,comida):
		self.nombre  = comida['nombre']
		self.peso  = comida['peso']
		self.datos = comida['datos'].copy()
		self.dieta = comida['dieta'].copy()
	
	def escalar (self, factor):
		dicc = {}
		for cualidad in self.datos:
			dicc[cualidad] = self.datos[cualidad]*factor
		return dicc


class comida:
	def __init__(self,Ingre):
		self.Ingre = Ingre.copy()
	
	def ask(self, cualidad):
		if cualidad in ['Veggie', 'Vegan']:
			cant = True
			for i in self.Ingre:
				cant = cant and i.dieta[cualidad]
			return cant
		else:
			cant = 0
			for i in self.Ingre:
				cant += i.datos[cualidad]
			return cant
	
	def forzar (self, patron):
		aux = []
		for i in self.Ingre:
			aux.append(list(i.datos.values()))
		M = Matrix(aux).T
		b = Matrix([list(patron.values())]).T
		d = M.gauss_jordan_solve(b)
		
		g = lambdify(d[1], d[0], modules='numpy')
		def f (parms):
			v = g(*parms)
			p = 1
'''			for i in v:
				if i < 0:
					p += 0.1 * abs(i)'''
			return p * norm(v, inf)
#		f = lambda parms : norm(g(*parms))
		init_guess = ones(len(d[1]))
		res = minimize(f, init_guess).x
		opt = g(*res)
		ing = self.Ingre
		lista = [(ing[i].nombre, opt[i]) for i in range(len(ing))]
		
		return lista

h = ingrediente(huevo)
l = ingrediente(leche)
p = ingrediente(pollo_pechuga)
m = ingrediente(manzana)
q = ingrediente(queso_cremoso)
a = ingrediente(arroz)

patron = almuerzo

c = comida([h,l,p,m,q,a])













